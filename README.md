# shopping-basket
By Amin Malekpour
---
# Technology, Tool and framework
These technologies, tools and frameworks have been used in the project:
1. Java 8
2. Spring Boot (Web, Test)
3. Gradle
4. SpringFox (swagger2, swagger-ui)
6. Lombok
---

# Data Layer
Considering the assignment's requirements, product catalogue is implemented as in-memory list, I followed the same pattern for other DAO classes, 
the in memory version of each DAO class is provided but it can be easily replace by other implementation like SQL or NoSQL. 

# Project structure
I have used Gradle as the build tool.
Source codes are located under the “shopping-basket/src/main/java” folder.
I used Project Lombok which is a java library that automatically plugs into your IDE and generating getter, setters, hashcode, equals and etc for your POJOs. please if you are using IDE like 
Intellij Idea consider installing lombok plugin for your IDE (https://plugins.jetbrains.com/plugin/6317-lombok-plugin)
---
# Document
REST API documentation is provided via “Swagger-UI”.
Use this link:   http://localhost:8080/swagger-ui.html
---
# Test
I have written some useful test classes under the “shopping-basket/src/test/java” folder. I used spring-boot-test
and JUnit to implement unit and integration tests. These tests were implemented by using facilities like
TestRestTemplate. If I had more time, I would implement more unit tests for controller, business and data layer.
---
# Run the application
Use the following commands to build and run the application:
1. gradle clean
2. gradle build
3. gradle bootRun
