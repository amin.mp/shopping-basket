package com.amin.shoppingbasket.controller;

import com.amin.shoppingbasket.service.BasketService;
import com.amin.shoppingbasket.service.dto.BasketDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@RestController
@RequestMapping(value = "/api/shoppingbasket")
@Api(description = "Provides set of REST API to handle CRUD operations for shopping basket.")
public class BasketController {

    @Autowired
    BasketService basketService;

    @GetMapping
    @ApiOperation("Retrieves all baskets.")
    public List<BasketDto> list() {
        return basketService.list();
    }

    @GetMapping("/{basketId}")
    @ApiOperation("Retrieves a basket by its basketId.")
    public BasketDto find(@PathVariable("basketId") Long basketId) {
        return basketService.find(basketId);
    }

    @PostMapping
    @ApiOperation("Create a new basket.")
    public ResponseEntity<BasketDto> insert() {
        return ResponseEntity.status(HttpStatus.CREATED).body(basketService.insert());
    }


    @PutMapping("/{basketId}/{productId}")
    @ApiOperation("Add a new product to an existing basket, if the product is existing in the basket then the quantity of product will be updated.")
    public ResponseEntity<BasketDto> update(@PathVariable("basketId") Long basketId,
                            @PathVariable("productId") Long productId,
                            @RequestParam("quantity") Integer quantity) {
        return ResponseEntity.status(HttpStatus.CREATED).body(basketService.update(basketId, productId, quantity));
    }

    @DeleteMapping("/{basketId}")
    @ApiOperation("Delete a basket by its basketId.")
    public ResponseEntity<?> delete(@PathVariable("basketId") Long basketId) {
        basketService.delete(basketId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{basketId}/{productId}")
    @ApiOperation("Delete a product from a basket by its basketId.")
    public ResponseEntity<?> delete(@PathVariable("basketId") Long basketId,
                                    @PathVariable("productId") Long productId) {
        basketService.delete(basketId, productId);
        return ResponseEntity.noContent().build();
    }
}
