package com.amin.shoppingbasket.controller;

import com.amin.shoppingbasket.service.ProductService;
import com.amin.shoppingbasket.service.dto.ProductDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@RestController
@RequestMapping(value = "/api/product")
@Api(description = "Provides set of REST API to handle CRUD operations for products.")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping
    @ApiOperation("Retrieves all products.")
    public List<ProductDto> list(){
        return productService.list();
    }

    @GetMapping("/{productId}")
    @ApiOperation("Retrieves a product by its is.")
    public ProductDto find(@PathVariable("productId") Long productId) {
        return productService.find(productId);
    }
}
