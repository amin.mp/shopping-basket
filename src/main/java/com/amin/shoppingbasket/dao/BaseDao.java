package com.amin.shoppingbasket.dao;

import com.amin.shoppingbasket.exception.DataLayerException;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
public interface BaseDao<Model> {

    /**
     * Find a record by basketId.
     * @param id
     * @return a record.
     */
    Model find(Long id) throws DataLayerException;

    /**
     * Get all records.
     * @return all records.
     */
    List<Model> list() throws DataLayerException;

    /**
     * Insert new record.
     * @param model
     * @return inserted record.
     */
    Model insert(Model model) throws DataLayerException;

    /**
     * Update an existing record.
     * @param model
     * @return updated record.
     */
    Model update(Model model) throws DataLayerException;

    /**
     * Delete a record by id.
     * @param id
     */
    void delete(Long id) throws DataLayerException;
}
