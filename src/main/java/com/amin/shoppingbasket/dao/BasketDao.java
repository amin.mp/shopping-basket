package com.amin.shoppingbasket.dao;

import com.amin.shoppingbasket.dao.model.Basket;
import com.amin.shoppingbasket.dao.model.BasketItem;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
public interface BasketDao extends BaseDao<Basket> {

    /**
     * Use this method for inserting a new Product or updating the quantity of an existing Product in a Basket.
     * @param basketId
     * @param productId
     * @param quantity
     * @return updated Basket.
     */
    Basket insertOrUpdateProduct(Long basketId, Long productId, Integer quantity);

    /**
     * Use this method for finding a BasketItem in a Basket by productId.
     * @param basketId
     * @param productId
     * @return BasketItem
     */
    BasketItem findBasketItem(Long basketId, Long productId);

}
