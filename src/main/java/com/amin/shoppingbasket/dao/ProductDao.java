package com.amin.shoppingbasket.dao;

import com.amin.shoppingbasket.dao.model.Product;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
public interface ProductDao extends BaseDao<Product> {
}
