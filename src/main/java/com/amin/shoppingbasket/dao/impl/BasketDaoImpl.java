package com.amin.shoppingbasket.dao.impl;

import com.amin.shoppingbasket.dao.BasketDao;
import com.amin.shoppingbasket.dao.ProductDao;
import com.amin.shoppingbasket.dao.model.Basket;
import com.amin.shoppingbasket.dao.model.BasketItem;
import com.amin.shoppingbasket.dao.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 * IMPORTANT: This is an In Memory implementation for BasketDao.
 */
@Repository
public class BasketDaoImpl implements BasketDao {

    @Autowired
    private ProductDao productDao;
    private Map<Long, Basket> map;
    private AtomicLong basketId;
    private AtomicLong itemId;

    public BasketDaoImpl() {
        map = new HashMap<>();
        basketId = new AtomicLong(0);
        itemId = new AtomicLong(0);
    }

    @Override
    public Basket find(Long basketId) {
        return map.get(basketId);
    }

    @Override
    public List<Basket> list() {
        return new ArrayList<>(map.values());
    }

    @Override
    public Basket insert(Basket basket) {
        Long id = basketId.incrementAndGet();
        basket.setId(id);
        map.put(id, basket);
        return map.get(id);
    }

    @Override
    public Basket update(Basket basket) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long basketId) {
        Basket basket = find(basketId);
        basket.getItems().forEach(item -> item.getProduct().updateStock(-item.getQuantity()));
        map.remove(basketId);
    }

    @Override
    public Basket insertOrUpdateProduct(Long basketId, Long productId, Integer quantity) {
        Basket basket = find(basketId);
        Product product = productDao.find(productId);
        BasketItem basketItem = findBasketItem(basketId, productId);
        product.updateStock(quantity - orZero(basketItem));
        if (basketItem == null) {
            if (quantity > 0) {
                BasketItem newBasketItem1 = new BasketItem(itemId.incrementAndGet(), product, quantity);
                basket.getItems().add(newBasketItem1);
            }
        } else {
            if(quantity > 0) {
                basketItem.setQuantity(quantity);
            } else {
                basket.getItems().remove(basketItem);
            }
        }
        return basket;
    }

    @Override
    public BasketItem findBasketItem(Long basketId, Long productId) {
        Basket basket = find(basketId);
        return basket.getItems().stream().filter(item -> productId.equals(item.getProduct().getId())).findAny().orElse(null);
    }

    private Integer orZero(BasketItem basketItem) {
        if(basketItem == null){
            return 0;
        }
        return basketItem.getQuantity();
    }
}
