package com.amin.shoppingbasket.dao.impl;

import com.amin.shoppingbasket.dao.ProductDao;
import com.amin.shoppingbasket.dao.model.Product;
import com.amin.shoppingbasket.exception.DataLayerException;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 * IMPORTANT: This is an In Memory implementation for ProductDao.
 */
@Repository
public class ProductDaoImpl implements ProductDao {

    private Map<Long, Product> map;
    private AtomicLong id;

    public ProductDaoImpl() {
        map = new HashMap<>();
        id = new AtomicLong(0);
    }

    @PostConstruct
    public void createProductCatalogue() {
        insert(new Product(null, "Apple iPhone X64GB", 800d, "Apple iPhone X 64GB - Black", 10));
        insert(new Product(null, "Samsung Galaxy Note 9 128GB", 900d, "Samsung Galaxy Note 9 128GB - Blue", 10));
        insert(new Product(null, "Huawei Honor Play 64GB Dual Sim", 230d, "Huawei Honor Play 64GB Dual Sim - Silver", 10));
        insert(new Product(null, "Motorola Moto G6 64GB", 120d, "Motorola Moto G6 64GB - Red", 10));
        insert(new Product(null, "Nokia 7 Plus 64GB", 200d, "Nokia 7 Plus 64GB - White", 10));
        insert(new Product(null, "Case for Apple iPhone X", 30d, "Case for  iPhone X - Black", 10));
        insert(new Product(null, "Case for Samsung Galaxy Note 9", 25d, "Case for Samsung Galaxy Note 9 - Blue", 10));
        insert(new Product(null, "Case for Huawei Honor Play", 15d, "Case for Huawei Honor Play - Silver", 10));
        insert(new Product(null, "Case for Motorola Moto G6", 18d, "Case for Motorola Moto G6 - Red", 10));
        insert(new Product(null, "Case for Nokia 7 Plus", 12d, "Case for Nokia 7 Plus - White", 10));
    }

    @Override
    public Product find(Long productId) {
        return map.get(productId);
    }

    @Override
    public List<Product> list() {
        return new ArrayList<>(map.values());
    }

    @Override
    public Product insert(Product product) {
        Long id = this.id.incrementAndGet();
        product.setId(id);
        map.put(id, product);
        return map.get(id);
    }

    @Override
    public Product update(Product product) throws DataLayerException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long productId) {
        throw new UnsupportedOperationException();
    }

}
