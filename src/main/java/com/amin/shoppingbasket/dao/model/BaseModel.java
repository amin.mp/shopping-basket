package com.amin.shoppingbasket.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Data
@EqualsAndHashCode(of = {"id"})
public class BaseModel {
    private Long id;
}
