package com.amin.shoppingbasket.dao.model;

import lombok.Data;

import java.util.Set;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Data
public class Basket extends BaseModel {
    Set<BasketItem> items;

    public Basket(Long id, Set<BasketItem> items) {
        setId(id);
        this.items = items;
    }
}
