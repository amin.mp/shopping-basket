package com.amin.shoppingbasket.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Data
@EqualsAndHashCode(of = {"product"})
public class BasketItem extends BaseModel {
    Product product;
    Integer quantity;

    public BasketItem(Long id, Product product, Integer quantity) {
        setId(id);
        setProduct(product);
        setQuantity(quantity);
    }
}
