package com.amin.shoppingbasket.dao.model;

import com.amin.shoppingbasket.exception.RunOutOfStock;
import lombok.Data;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Data
public class Product extends BaseModel {
    private String name;
    private Double price;
    private String description;
    private AtomicInteger stock;

    public Product(Long id, String name, Double price, String description, Integer stock) {
        setId(id);
        this.name = name;
        this.price = price;
        this.description = description;
        this.stock = new AtomicInteger(stock);
    }

    public void updateStock(Integer requestedQuantity) {
        getStock().getAndUpdate(value -> {
            if (value > requestedQuantity) {
                return value - requestedQuantity;
            } else {
                throw new RunOutOfStock(String.format("\"%s\" is out of stock", this.name));
            }
        });
    }
}
