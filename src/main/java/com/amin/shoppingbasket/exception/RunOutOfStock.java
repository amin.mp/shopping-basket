package com.amin.shoppingbasket.exception;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Feb 2019
 */
public class RunOutOfStock extends RuntimeException {

    public RunOutOfStock(String message) {
        super(message);
    }
}
