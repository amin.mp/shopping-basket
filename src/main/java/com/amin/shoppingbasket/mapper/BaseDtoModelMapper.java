package com.amin.shoppingbasket.mapper;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
public interface BaseDtoModelMapper<DTO, MODEL> {

    /**
     * Map set of models to set of DTOs.
     * @param models
     * @return set of DTOs.
     */
    default Set<DTO> mapToDtoSet(final Set<MODEL> models) {
        return models == null ? null : models.stream().map(this::mapToDto).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Map set of DTOs to set of models.
     * @param dtos
     * @return set of models.
     */
    default Set<MODEL> mapToModelSet(final Set<DTO> dtos) {
        return dtos == null ? null : dtos.stream().map(this::mapToModel).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Map list of models to list of DTOs.
     * @param models
     * @return list of DTOs
     */
    default List<DTO> mapToDtoList(final List<MODEL> models) {
        return models == null ? null : models.stream().map(this::mapToDto).collect(toList());
    }

    /**
     * Map list of DTOs to list of models.
     * @param dtos
     * @return list of models.
     */
    default List<MODEL> mapToModelList(final List<DTO> dtos) {
        return dtos == null ? null : dtos.stream().map(this::mapToModel).collect(toList());
    }

    /**
     * Map a model to a DTO.
     * @param model
     * @return a DTO.
     */
    DTO mapToDto(MODEL model);

    /**
     * Map a DTO to a model.
     * @param dto
     * @return a model.
     */
    MODEL mapToModel(DTO dto);
}
