package com.amin.shoppingbasket.mapper.impl;

import com.amin.shoppingbasket.dao.model.BasketItem;
import com.amin.shoppingbasket.mapper.BaseDtoModelMapper;
import com.amin.shoppingbasket.service.dto.BasketItemDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Component
public class BasketItemMapper implements BaseDtoModelMapper<BasketItemDto, BasketItem> {

    @Autowired
    private BasketProductMapper basketProductMapper;

    @Override
    public BasketItemDto mapToDto(BasketItem model) {
        if(model == null) return null;
        return new BasketItemDto(model.getId(), basketProductMapper.mapToDto(model.getProduct()), model.getQuantity());
    }

    @Override
    public BasketItem mapToModel(BasketItemDto dto) {
        if(dto == null) return null;
        return new BasketItem(dto.getItemId(), basketProductMapper.mapToModel(dto.getProduct()), dto.getQuantity());
    }
}
