package com.amin.shoppingbasket.mapper.impl;

import com.amin.shoppingbasket.dao.model.Basket;
import com.amin.shoppingbasket.mapper.BaseDtoModelMapper;
import com.amin.shoppingbasket.service.dto.BasketDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Component
public class BasketMapper implements BaseDtoModelMapper<BasketDto, Basket> {

    @Autowired
    private BasketItemMapper basketItemMapper;

    @Override
    public BasketDto mapToDto(Basket model) {
        if(model == null) return null;
        return new BasketDto(model.getId(), basketItemMapper.mapToDtoSet(model.getItems()));
    }

    @Override
    public Basket mapToModel(BasketDto dto) {
        if(dto == null) return null;
        return new Basket(dto.getBasketId(), basketItemMapper.mapToModelSet(dto.getItems()));
    }
}
