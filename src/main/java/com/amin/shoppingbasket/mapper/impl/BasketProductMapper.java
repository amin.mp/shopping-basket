package com.amin.shoppingbasket.mapper.impl;

import com.amin.shoppingbasket.dao.model.Product;
import com.amin.shoppingbasket.mapper.BaseDtoModelMapper;
import com.amin.shoppingbasket.service.dto.BasketProductDto;
import org.springframework.stereotype.Component;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Component
public class BasketProductMapper implements BaseDtoModelMapper<BasketProductDto, Product> {

    @Override
    public BasketProductDto mapToDto(Product model) {
        if(model == null) return null;
        return new BasketProductDto(model.getId(), model.getName(),
                model.getPrice(), model.getDescription());
    }

    @Override
    public Product mapToModel(BasketProductDto dto) {
        if(dto == null) return null;
        return new Product(dto.getProductId(), dto.getName(),
                dto.getPrice(), dto.getDescription(), 0);
    }
}
