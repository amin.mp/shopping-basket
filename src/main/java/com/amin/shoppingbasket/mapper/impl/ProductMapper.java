package com.amin.shoppingbasket.mapper.impl;

import com.amin.shoppingbasket.dao.model.Product;
import com.amin.shoppingbasket.mapper.BaseDtoModelMapper;
import com.amin.shoppingbasket.service.dto.ProductDto;
import org.springframework.stereotype.Component;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Component
public class ProductMapper implements BaseDtoModelMapper<ProductDto, Product> {

    @Override
    public ProductDto mapToDto(Product model) {
        if(model == null) return null;
        return new ProductDto(model.getId(), model.getName(),
                model.getPrice(), model.getDescription(), model.getStock().get());
    }

    @Override
    public Product mapToModel(ProductDto dto) {
        if(dto == null) return null;
        return new Product(dto.getProductId(), dto.getName(),
                dto.getPrice(), dto.getDescription(), dto.getStock());
    }
}
