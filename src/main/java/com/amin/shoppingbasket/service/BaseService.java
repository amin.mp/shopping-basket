package com.amin.shoppingbasket.service;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
public interface BaseService<Dto> {

    /**
     * Add new Dto.
     * @param dto
     * @return Dto
     */
    Dto insert(Dto dto);

    /**
     * Delete an existing Dto by id.
     * @param id
     */
    void delete(Long id);

    /**
     * Get all DTOs.
     * @return list of DTOs
     */
    List<Dto> list();

    /**
     * Find a Dto by id.
     * @param id
     * @return
     */
    Dto find(Long id);
}
