package com.amin.shoppingbasket.service;

import com.amin.shoppingbasket.service.dto.BasketDto;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
public interface BasketService extends BaseService<BasketDto> {

    /**
     * Create a new Basket.
     * @return created BasketDto
     */
    BasketDto insert();

    /**
     * Update a Basket.
     * @param productId
     * @return updated BasketDto
     */
    BasketDto update(Long basketId, Long productId, Integer quantity);

    /**
     * Delete a Product from a Basket.
     * @param basketId
     * @param productId
     */
    void delete(Long basketId, Long productId);
}
