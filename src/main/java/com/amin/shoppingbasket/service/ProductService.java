package com.amin.shoppingbasket.service;

import com.amin.shoppingbasket.service.dto.ProductDto;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
public interface ProductService extends BaseService<ProductDto> {
}
