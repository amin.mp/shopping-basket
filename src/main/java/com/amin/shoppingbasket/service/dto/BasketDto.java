package com.amin.shoppingbasket.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasketDto implements Serializable {
    Long basketId;
    Set<BasketItemDto> items;
}
