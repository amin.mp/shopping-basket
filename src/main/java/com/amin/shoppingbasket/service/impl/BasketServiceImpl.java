package com.amin.shoppingbasket.service.impl;

import com.amin.shoppingbasket.dao.BasketDao;
import com.amin.shoppingbasket.dao.ProductDao;
import com.amin.shoppingbasket.dao.model.Basket;
import com.amin.shoppingbasket.dao.model.BasketItem;
import com.amin.shoppingbasket.dao.model.Product;
import com.amin.shoppingbasket.exception.InvalidRequestException;
import com.amin.shoppingbasket.exception.NotFoundException;
import com.amin.shoppingbasket.exception.RunOutOfStock;
import com.amin.shoppingbasket.mapper.impl.BasketMapper;
import com.amin.shoppingbasket.service.BasketService;
import com.amin.shoppingbasket.service.dto.BasketDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Service
public class BasketServiceImpl implements BasketService {

    @Autowired
    private BasketMapper basketMapper;

    @Autowired
    private BasketDao basketDao;

    @Autowired
    private ProductDao productDao;

    @Override
    public BasketDto insert(BasketDto basketDto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public BasketDto insert() {
        Basket basket = new Basket(null, new LinkedHashSet<>());
        return basketMapper.mapToDto(basketDao.insert(basket));
    }

    @Override
    public BasketDto update(Long basketId, Long productId, Integer quantity) {
        validateBeforeUpdate(basketId, productId, quantity);
        Basket basket = basketDao.insertOrUpdateProduct(basketId, productId, quantity);
        return basketMapper.mapToDto(basket);
    }

    @Override
    public void delete(Long basketId) {
        basketDao.delete(basketId);
    }

    @Override
    public void delete(Long basketId, Long productId) {
        basketDao.insertOrUpdateProduct(basketId, productId, 0);
    }

    @Override
    public List<BasketDto> list() {
        return basketMapper.mapToDtoList(basketDao.list());
    }

    @Override
    public BasketDto find(Long basketId) {
        Basket basket = basketDao.find(basketId);
        if (basket == null) throw new NotFoundException(String.format("There is no Basket with basketId = %s", basketId));
        return basketMapper.mapToDto(basket);
    }

    private void validateBeforeUpdate(Long basketId, Long productId, Integer quantity) {
        Basket basket = basketDao.find(basketId);
        if(basket == null) throw new InvalidRequestException("basketId is not valid.");
        Product product = productDao.find(productId);
        if(product == null) throw new InvalidRequestException("productId is not valid.");
        if(quantity < 0) throw new InvalidRequestException("quantity is not valid.");
        Integer requestedQuantity = quantity - orZero(basketDao.findBasketItem(basketId, productId));
        if(product.getStock().get() < requestedQuantity) throw new RunOutOfStock(String.format("\"%s\" is out of stock.", product.getName()));
    }

    private Integer orZero(BasketItem basketItem) {
        if(basketItem == null){
            return 0;
        }
        return basketItem.getQuantity();
    }
}
