package com.amin.shoppingbasket.service.impl;

import com.amin.shoppingbasket.dao.ProductDao;
import com.amin.shoppingbasket.dao.model.Product;
import com.amin.shoppingbasket.exception.NotFoundException;
import com.amin.shoppingbasket.mapper.impl.ProductMapper;
import com.amin.shoppingbasket.service.ProductService;
import com.amin.shoppingbasket.service.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 09 Feb 2019
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ProductMapper productMapper;

    @Override
    public ProductDto insert(ProductDto productDto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long productId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<ProductDto> list() {
        return productMapper.mapToDtoList(productDao.list());
    }

    @Override
    public ProductDto find(Long productId) {
        Product product = productDao.find(productId);
        if(product == null) throw new NotFoundException(String.format("There is no Product with productId = %s", productId));
        return productMapper.mapToDto(product);
    }
}
