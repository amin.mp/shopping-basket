package com.amin.shoppingbasket;

import com.amin.shoppingbasket.controller.BasketController;
import com.amin.shoppingbasket.controller.ProductController;
import com.amin.shoppingbasket.dao.BasketDao;
import com.amin.shoppingbasket.dao.ProductDao;
import com.amin.shoppingbasket.service.BasketService;
import com.amin.shoppingbasket.service.ProductService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ShoppingBasketApplicationTests {

	@Autowired
	private ApplicationContext applicationContext;

	@Test
	public void contextLoads() {
		Assert.assertNotNull(applicationContext.getBean(ProductController.class));
		Assert.assertNotNull(applicationContext.getBean(BasketController.class));

		Assert.assertNotNull(applicationContext.getBean(ProductService.class));
		Assert.assertNotNull(applicationContext.getBean(BasketService.class));

		Assert.assertNotNull(applicationContext.getBean(ProductDao.class));
		Assert.assertNotNull(applicationContext.getBean(BasketDao.class));
	}

}

