package com.amin.shoppingbasket.controller;

import com.amin.shoppingbasket.dao.BasketDao;
import com.amin.shoppingbasket.dao.ProductDao;
import com.amin.shoppingbasket.dao.model.Basket;
import com.amin.shoppingbasket.dao.model.BasketItem;
import com.amin.shoppingbasket.dao.model.Product;
import com.amin.shoppingbasket.service.dto.BasketDto;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Feb 2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BasketControllerIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private BasketDao basketDao;

    @Autowired
    private ProductDao productDao;

    private  Product product;

    private Long invalidProductId;

    @Before
    public void setUp(){
        deleteAllBaskets();
        product = productDao.list().get(0);
        int size = productDao.list().size();
        Product lastProduct = productDao.list().get(size - 1);
        invalidProductId = lastProduct.getId() + 1L;
    }

    @After
    public void cleanUp(){
        deleteAllBaskets();
    }

    @Test
    public void findAllBaskets_whenGetAndThereIsNoBasket_theReturnEmptyList() {
        ResponseEntity<List<BasketDto>> actualResponse = testRestTemplate.exchange("/api/shoppingbasket", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<BasketDto>>(){});

        List<BasketDto> actualBasketResponse = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.OK.value())));
        assertThat(actualBasketResponse, notNullValue());
        assertThat(actualBasketResponse, IsEmptyCollection.empty());
    }

    @Test
    public void findAllBaskets_whenGetAndBasketCreated_theReturnNotEmptyList() {
        //Before
        Basket basket = createBasketWithEmptyItem();

        //Test
        ResponseEntity<List<BasketDto>> actualResponse = testRestTemplate.exchange("/api/shoppingbasket", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<BasketDto>>(){});
        List<BasketDto> actualBasketResponse = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.OK.value())));
        assertThat(actualBasketResponse, notNullValue());
        assertThat(actualBasketResponse, hasSize(1));
        assertThat(actualBasketResponse.get(0).getBasketId(), equalTo(basket.getId()));
    }

    @Test
    public void createBasket_whenPost_thenReturnBasket() {
        //Test
        ResponseEntity<BasketDto> actualResponse = testRestTemplate.exchange("/api/shoppingbasket", HttpMethod.POST,
                null, BasketDto.class, "");

        BasketDto actualBasketResponse = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.CREATED.value())));
        assertThat(actualBasketResponse, notNullValue());
        assertThat(actualBasketResponse.getBasketId(), equalTo(basketDao.find(actualBasketResponse.getBasketId()).getId()));
        assertThat(actualBasketResponse.getItems(), IsEmptyCollection.empty());
    }

    @Test
    public void updateBasket_whenPutAndValidRequest_thenReturnBasket() {
        //Before
        Basket basket = createBasketWithEmptyItem();

        //Test
        ResponseEntity<BasketDto> actualResponse = testRestTemplate.exchange(
                String.format("/api/shoppingbasket/%s/%s?quantity=%s", basket.getId(), product.getId(), 1),
                HttpMethod.PUT,
                null, BasketDto.class);

        BasketDto actualBasketResponse = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.CREATED.value())));
        basket = basketDao.find(basket.getId());
        assertThat(actualBasketResponse, notNullValue());
        assertThat(actualBasketResponse.getBasketId(), equalTo(basket.getId()));
        assertThat(actualBasketResponse.getItems().iterator().next().getItemId(), equalTo(basket.getItems().iterator().next().getId()));
    }

    @Test
    public void updateBasket_whenPutAndInvalidBasketId_thenReturnBadRequest() {
        //Before
        Basket basket = createBasketWithEmptyItem();

        ResponseEntity<String> actualResponse = testRestTemplate.exchange(
                String.format("/api/shoppingbasket/%s/%s?quantity=%s", (basket.getId() + 1L), product.getId(), product.getStock()),
                HttpMethod.PUT,
                null, String.class);

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.BAD_REQUEST.value())));
    }

    @Test
    public void updateBasket_whenPutAndInvalidProductId_thenReturnBadRequest() {
        //Before
        Basket basket = createBasketWithEmptyItem();

        ResponseEntity<String> actualResponse = testRestTemplate.exchange(
                String.format("/api/shoppingbasket/%s/%s?quantity=%s", basket.getId(), invalidProductId, 1),
                HttpMethod.PUT,
                null, String.class);

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.BAD_REQUEST.value())));
    }

    @Test
    public void updateBasket_whenPutAndInvalidQuantity_thenReturnBadRequest() {
        //Before
        Basket basket = createBasketWithEmptyItem();

        //Test
        ResponseEntity<String> actualResponse = testRestTemplate.exchange(
                String.format("/api/shoppingbasket/%s/%s?quantity=%s", basket.getId(), product.getId(), (1 + product.getStock().intValue())),
                HttpMethod.PUT,
                null, String.class);

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.BAD_REQUEST.value())));
    }

    @Test
    public void deleteBasket_whenDeleteAndValidRequest_theReturnNoContent() {
        //Before
        Basket basket = createBasketWithEmptyItem();

        //Test
        ResponseEntity<String> actualResponse = testRestTemplate.exchange(
                String.format("/api/shoppingbasket/%s", basket.getId()), HttpMethod.DELETE,
                null, String.class);

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.NO_CONTENT.value())));
        assertThat(basketDao.find(basket.getId()), nullValue());
    }

    @Test
    public void deleteBasketItem_whenDeleteAndValidRequest_theReturnNoContent() {
        //Before
        Basket basket = createBasketWithOneItem();

        //Test
        ResponseEntity<String> actualResponse = testRestTemplate.exchange(
                String.format("/api/shoppingbasket/%s/%s", basket.getId(), product.getId()), HttpMethod.DELETE,
                null, String.class);

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.NO_CONTENT.value())));
        Basket updatedBasket = basketDao.find(basket.getId());
        assertThat(updatedBasket, notNullValue());
        assertThat(updatedBasket.getId(), equalTo(basket.getId()));
        assertThat(updatedBasket.getItems(), hasSize(0));
    }

    private void deleteAllBaskets(){
        basketDao.list().forEach(basket -> basketDao.delete(basket.getId()));
    }

    private Basket createBasketWithEmptyItem(){
        Basket basket = basketDao.insert(new Basket(null, new LinkedHashSet<>()));
        assertThat(basket, notNullValue());
        assertThat(basket.getId(), notNullValue());
        assertThat(basket.getId(), any(Long.class));
        return basket;
    }

    private Basket createBasketWithOneItem(){
        BasketItem basketItem = new BasketItem(null, product, 1);
        Set<BasketItem> basketItems = new LinkedHashSet<>();
        basketItems.add(basketItem);
        Basket basket = basketDao.insert(new Basket(null, basketItems));
        assertThat(basket, notNullValue());
        assertThat(basket.getId(), notNullValue());
        assertThat(basket.getId(), any(Long.class));
        assertThat(basket.getItems(), hasItem(basketItem));
        return basket;
    }

}
