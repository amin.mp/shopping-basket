package com.amin.shoppingbasket.controller;

import com.amin.shoppingbasket.service.BasketService;
import com.amin.shoppingbasket.service.dto.BasketDto;
import com.amin.shoppingbasket.service.dto.BasketItemDto;
import com.amin.shoppingbasket.service.dto.BasketProductDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 11 Feb 2019
 */
@RunWith(SpringRunner.class)
@WebMvcTest(BasketController.class)
public class BasketControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BasketService basketService;

    private BasketProductDto basketProductDto;

    @Before
    public void setUp(){
        basketProductDto = new BasketProductDto(1L,"iPhone",200d, "Black");
    }

    @Test
    public void findAllBaskets_whenGet_thenReturnEmptyList() throws Exception {
        List<BasketDto> expectedResult = new ArrayList<>();
        given(basketService.list()).willReturn(expectedResult);
        mvc.perform(get("/api/shoppingbasket"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    public void findAllBaskets_whenGet_thenReturnNotEmptyList() throws Exception {
        List<BasketDto> expectedResult = createBasketDtoList();
        given(basketService.list()).willReturn(expectedResult);
        mvc.perform(get("/api/shoppingbasket"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].basketId", is(expectedResult.get(0).getBasketId().intValue())))
                .andExpect(jsonPath("$[0].items[0].itemId", is(expectedResult.get(0).getItems().iterator().next().getItemId().intValue())))
                .andExpect(jsonPath("$[0].items[0].quantity", is(expectedResult.get(0).getItems().iterator().next().getQuantity())))
                .andExpect(jsonPath("$[0].items[0].product.productId", is(expectedResult.get(0).getItems().iterator().next().getProduct().getProductId().intValue())))
                .andExpect(jsonPath("$[0].items[0].product.name", is(expectedResult.get(0).getItems().iterator().next().getProduct().getName())))
                .andExpect(jsonPath("$[0].items[0].product.price", is(expectedResult.get(0).getItems().iterator().next().getProduct().getPrice())))
                .andExpect(jsonPath("$[0].items[0].product.description", is(expectedResult.get(0).getItems().iterator().next().getProduct().getDescription())));
    }

    private List<BasketDto> createBasketDtoList(){
        List<BasketDto> result = new ArrayList<>();
        BasketItemDto basketItemDto = new BasketItemDto(1L, basketProductDto, 1);
        Set<BasketItemDto> basketItemDtoSet = new LinkedHashSet<>();
        basketItemDtoSet.add(basketItemDto);
        BasketDto basketDto = new BasketDto(1L, basketItemDtoSet);
        result.add(basketDto);
        return result;
    }

}
