package com.amin.shoppingbasket.controller;

import com.amin.shoppingbasket.dao.ProductDao;
import com.amin.shoppingbasket.dao.model.Product;
import com.amin.shoppingbasket.service.dto.ProductDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Feb 2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private ProductDao productDao;

    private  Product firstProduct;

    private Long invalidProductId;

    @Before
    public void setUp(){
        firstProduct = productDao.list().get(0);
        int size = productDao.list().size();
        Product lastProduct = productDao.list().get(size - 1);
        invalidProductId = lastProduct.getId() + 1L;
    }

    @Test
    public void findProducts_whenGet_thenReturnProductList(){
        ResponseEntity<List<ProductDto>> actualResponse = testRestTemplate.exchange("/api/product", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<ProductDto>>(){});

        List<ProductDto> actualProductResponse = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.OK.value())));
        assertThat(actualProductResponse, notNullValue());
        assertThat(actualProductResponse, hasSize(10));
        assertThat(actualProductResponse.get(0).getProductId(), notNullValue());
        assertThat(actualProductResponse.get(0).getProductId(), any(Long.class));
    }

    @Test
    public void findProduct_whenGet_thenReturnProduct(){
        ResponseEntity<ProductDto> actualResponse = testRestTemplate.exchange(
                String.format("/api/product/%s", firstProduct.getId()), HttpMethod.GET,
                null, new ParameterizedTypeReference<ProductDto>(){});

        ProductDto actualProductResponse = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.OK.value())));
        assertThat(actualProductResponse, notNullValue());
        assertThat(actualProductResponse.getProductId(), equalTo(firstProduct.getId()));
    }

    @Test
    public void findProduct_whenGet_thenReturnNotFound(){
        ResponseEntity<ProductDto> actualResponse = testRestTemplate.exchange(
                String.format("/api/firstProduct/%s", invalidProductId), HttpMethod.GET,
                null, new ParameterizedTypeReference<ProductDto>(){});

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.NOT_FOUND.value())));
    }

}
