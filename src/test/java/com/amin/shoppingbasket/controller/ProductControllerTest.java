package com.amin.shoppingbasket.controller;

import com.amin.shoppingbasket.service.ProductService;
import com.amin.shoppingbasket.service.dto.ProductDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 11 Feb 2019
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService productService;

    @Test
    public void findAllProducts_whenGet_thenReturnNotEmptyList() throws Exception {
        List<ProductDto> expectedResult = createProductDtoList();
        given(productService.list()).willReturn(expectedResult);
        mvc.perform(get("/api/product"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].productId", is(expectedResult.get(0).getProductId().intValue())))
                .andExpect(jsonPath("$[0].name", is(expectedResult.get(0).getName())))
                .andExpect(jsonPath("$[0].price", is(expectedResult.get(0).getPrice())))
                .andExpect(jsonPath("$[0].description", is(expectedResult.get(0).getDescription())));
    }

    @Test
    public void findProduct_whenGet_thenReturnProduct() throws Exception {
        List<ProductDto> expectedResult = createProductDtoList();
        given(productService.list()).willReturn(expectedResult);
        mvc.perform(get("/api/product"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].productId", is(expectedResult.get(0).getProductId().intValue())))
                .andExpect(jsonPath("$[0].name", is(expectedResult.get(0).getName())))
                .andExpect(jsonPath("$[0].price", is(expectedResult.get(0).getPrice())))
                .andExpect(jsonPath("$[0].description", is(expectedResult.get(0).getDescription())));
    }

    private List<ProductDto> createProductDtoList(){
        List<ProductDto> result = new ArrayList<>();
        ProductDto productDto = new ProductDto(1L, "Apple iPhone X64GB", 800d, "Apple iPhone X 64GB - Black", 10);
        result.add(productDto);
        return result;
    }
}
